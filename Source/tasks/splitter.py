"""
DataSplitter class with data partition.
"""

from sklearn.cross_validation import train_test_split

from .loader import DataLoader


class DataSplitter(object):
    """
    Gathers the label data and message data.
    Separates data into 80:20 proportion.
    80 to training_data and 20 to testing data.
    """
    def data_split(self):
        """

        :return:
        """

        data = DataLoader().csv_reader()

        data_labels = [int(item[0]) for item in data]
        data_messages = [item[1] for item in data]
        training_message, testing_message, training_label, testing_label = train_test_split(data_messages, data_labels,
                                                                                            test_size=0.20)
        return training_message, testing_message, training_label, testing_label

