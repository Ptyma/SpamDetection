"""
Classification class with classifier and visualization of predicted data.
"""

from .vectorizer import DataVectorizer

from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score,precision_score,recall_score,f1_score
import matplotlib.pyplot as plt
import numpy as np


class Classification(object):
    def __init__(self):
        """

        """

        self.clfObj = MultinomialNB()
        self.tr_vector, self.te_vector, self.training_label, self.testing_label = DataVectorizer().data_vectorization()
        self.pred = None

    def train_classifier(self):
        """
        Fits the data to MultinomialNB
        :return:
        """

        self.clfObj.fit(self.tr_vector, self.training_label)

    def score(self):
        """
        Does the calculations.
        :return:
        """

        self.pred = self.clfObj.predict(self.te_vector)

        print('Accuracy score: {}'.format(accuracy_score(self.testing_label, self.pred)))
        print('Precision score: {}'.format(precision_score(self.testing_label, self.pred)))
        print('Recall score: {}'.format(recall_score(self.testing_label, self.pred)))
        print('F1 score: {}'.format(f1_score(self.testing_label, self.pred)))

        self.visualization()

    def visualization(self):
        """
        Creates a bar graph for the predicted data.
        :return:
        """
        spamCount = len([item for item in self.pred if item==1])
        hamCount = len([item for item in self.pred if item==0])

        n_groups = 1
        means_spam = (spamCount)
        means_ham = (hamCount)
        plt.subplots()
        index = np.arange(n_groups)
        bar_width = 0.10
        opacity = 0.8
        plt.bar(index, means_spam, bar_width,
                         alpha=opacity,
                         color='b',
                         label='Spam')

        plt.bar(index + bar_width, means_ham, bar_width,
                         alpha=opacity,
                         color='g',
                         label='Ham')
        plt.xlabel('Types')
        plt.ylabel('Data-Count')
        plt.title('Predicted by Algorithm')
        plt.xticks([])
        plt.legend()

        plt.tight_layout()
        plt.show()