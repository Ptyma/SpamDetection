"""
DataLoader class with data reading function
"""

import csv


class DataLoader(object):
    """
    Reads the csv file 'data.csv' and appends it to a list excluding the headers.
    """
    def csv_reader(self):
        """

        :return:
        :type:
        """

        test_list = []
        with open('./data/data.csv') as data_file:
            data_reader = csv.reader(data_file, delimiter='\t')
            for rows in data_reader:
                test_list.append(rows)
        return test_list[1:]