"""
DataVectorizer class with vectorization function
"""

import string
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk import corpus

from .splitter import DataSplitter


class DataVectorizer(object):
    """
    Removes stopwords, punctuations.
    Creates tf-idf vectors.
    """
    def data_vectorization(self):
        """

        :return:
        """

        training_message, testing_message, training_label, testing_label = DataSplitter().data_split()
        vecObj = TfidfVectorizer(stop_words=corpus.stopwords.words('english') + list(string.punctuation))
        tr_vector = vecObj.fit_transform(training_message)
        te_vector = vecObj.transform(testing_message)

        return tr_vector, te_vector, training_label, testing_label